#!/bin/bash

postfix -v start

/usr/sbin/opendkim -x /etc/opendkim.conf -u opendkim

touch /var/log/mail.log

tail -f /var/log/mail.log
