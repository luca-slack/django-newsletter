from django.conf.urls import url
from django.contrib import admin
from newsletter import views
admin.autodiscover()

admin.site.site_header = 'Amministrazione Mailer'
admin.site.site_title = 'Mailer'

urlpatterns = [
    url(r'^u/(?P<domain>[\w-]+)/(?P<s_uuid>[\w-]+)/', views.unsubscribe, name="unsubscribe"),
    url(r'^v/(?P<newsletter_uuid>[\w-]+)/',           views.viewer, name="view"),
    url(r'^disable/',                                 views.import_and_disable, name="bulk_disable"),
    url(r'^import/',                                  views.subscriber_import, name="bulk_import"),
    url(r'^export/(?P<gid>[\d-]+)/',                  views.subscriber_export, name="bulk_export"),
    url(r'^metrics/(?P<newsletter_uuid>[\d-]+)/',     views.NewsLetterMetricView.as_view(), name="metrics"),
    url(r'^', admin.site.urls),
]
