"""
Django settings for mailer project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


SECRET_KEY = '_u4(6g76#27t^)@b+vvih4$kh@xmzs7+2qw@csvn%!*f*^paq_'

DEBUG = bool( os.getenv('DEBUG', False) )
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']

# Postfix config (docker smtp) ################################################
# EMAIL_HOST = 'postfix-smtp'
# EMAIL_PORT = 25
# EMAIL_TIMEOUT = 10
# SERVER_EMAIL = 'no-reply@cloudun.org'

# Postfix config (aruba) ######################################################
EMAIL_HOST = 'mail.jmproduction.it'
EMAIL_HOST_PASSWORD = 'manuelcl19'
EMAIL_HOST_USER = 'info@jmproduction.it'
EMAIL_PORT = 25
EMAIL_TIMEOUT = 10
SERVER_EMAIL = 'info@jmproduction.it'

# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'cloudun',
    'newsletter',
)

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

ROOT_URLCONF = 'mailer.urls'

WSGI_APPLICATION = 'mailer.wsgi.application'


# Database
DATABASES = {
    'default': {
        'ENGINE'    : 'django.db.backends.mysql',
        'NAME'      : os.getenv('DB_NAME'),
        'HOST'      : os.getenv('DB_HOST'),
        'PASSWORD'  : os.getenv('DB_PASS'),
        'PORT'      : os.getenv('DB_PORT'),
        'USER'      : os.getenv('DB_USER'),
        'CHARSET'   : 'utf8mb4',
        'COLLATION' : 'utf8mb4_unicode_ci',

        # Specific options for MySqlDb
        'DATABASE_OPTIONS' : {
            'connect_timeout' : 10,
        },
    }
}

# Internationalization
LANGUAGE_CODE = 'it-it'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Newsletter Federico
ADMINS = (
    ('Luca Contini', 'jkd.luca@gmail.com'),
)

MANAGERS = ADMINS

BASE_URL = os.getenv('BASE_URL')

MEDIA_ROOT = '/srv/media/mailer/'
MEDIA_URL = '/media/'

STATIC_ROOT = '/srv/static/mailer/'
STATIC_URL = '/static/'
