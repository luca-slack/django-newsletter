from django.contrib import messages
from django.db import transaction
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import redirect, render, get_object_or_404
from django.views.decorators.csrf import csrf_protect
from django.views.generic.list import ListView
from newsletter.models import Subscriber, SenderDomain, NewsLetterGroup, NewsLetterHandler, NewsLetterMetric
import re

# TODO
# class ImportView(FormView):
#     def dispatch(self, request, *args, **kwargs):
#         # Check
#         if request.user.is_staff:
#             return super(ImportView, self).dispatch(request, *args, **kwargs)
#         else:
#             return HttpResponseForbidden()


@csrf_protect
def import_and_disable(request):
    """Disable all mail addresses specified."""

    if not request.user.is_staff:
        return HttpResponseForbidden()

    # Template object
    context = { "title" : "Sospensione indirizzi" }

    if request.method == "POST":
        # Address list
        address_list = re.split("[\s,;]+", request.POST.get("addr", "").lower())

        # Unloaded addresses
        failures = []

        with transaction.atomic():
            # For each non-empty address
            for addr in address_list:
                if addr == "":
                    continue

                try:
                    # Get or create subscriber
                    subscriber, created = Subscriber.objects.get_or_create(email=addr, defaults={"exists" : False})

                    # Save
                    subscriber.save()

                    # Disable
                    subscriber.disable(reason="Marcato come disattivo tramite azione \"Importa e disabilita\" eseguita da %s" % request.user.get_full_name())
                except:
                    failures.append(addr)

        # To message framework
        if len(failures) > 0:
            messages.add_message(request, messages.WARNING, "Operazione completata! Gli indirizzi riportati sono stati ignorati.")
        else:
            messages.add_message(request, messages.SUCCESS, "Operazione completata con successo!")

        # Update request context
        context["failed"] = failures

    # Template loader
    return render(request, "import.html", context=context)


@csrf_protect
def subscriber_import(request):
    """Import all mail addresses into each specified group."""
    if not request.user.is_staff:
        return HttpResponseForbidden()

    # Template object
    context = { "title" : "Import indirizzi" }

    # GET, first step of import
    if request.method == "GET":
        context["step"] = 1
        context["groups_id"] = request.GET.get("groups_id", "-1")

        return render(request, "import.html", context=context)

    # POST, second step of import
    context["step"] = 2
    context["groups_id"] = request.POST.get("groups_id", "")

    # Selected groups
    groups_id = request.POST.get("groups_id", "").split(",")
    groups = NewsLetterGroup.objects.filter(id__in=[ int(gid) for gid in groups_id ])

    # Lowercase addresses
    address_list = request.POST.get("addr", "").lower()

    # Convert to address list
    address_list = re.split("[\s,;]+", address_list)

    # Unloaded addresses
    failures = []

    # For each non-empty address
    with transaction.atomic():
        for addr in address_list:
            if addr == "":
                continue

            try:
                # Get or create subscriber
                subscriber, created = Subscriber.objects.get_or_create(email=addr, defaults={"exists" : True})
                subscriber.groups.add(*groups)
            except:
                failures.append(addr)

    # To message framework
    if len(failures) > 0:
        messages.add_message(request, messages.WARNING, "Operazione completata! Gli indirizzi riportati sono stati ignorati.")
    else:
        messages.add_message(request, messages.SUCCESS, "Operazione completata con successo!")

    # Update request context
    context["failed"] = failures

    # Template loader
    return render(request, "import.html", context=context)


def subscriber_export(request, gid):
    """Subscriber export procedure."""
    if not request.user.is_staff:
        return HttpResponseForbidden()

    # Parent group
    group = get_object_or_404(NewsLetterGroup, pk=gid)

    # Querysets to export
    # Luca 2013-03-14: add prefetch_related to speed up export
    subscribers = group.subscribers.all().prefetch_related("nodomains")
    domains = SenderDomain.objects.all()

    # Template loader for table generator
    table_response = render(request, "export.html", context={
        "domains" : domains,
        "subscribers" : subscribers,
    })

    # Let the client use this as ms-excel sheet
    table_response["Content-Type"] = "application/vnd.ms-excel"
    table_response["Content-Disposition"] = "attachment; filename=\"%s.xls\"" % group.name

    return table_response


def unsubscribe(request, domain, s_uuid):
    """Unsubscribe user from a particular domain."""
    # Get subscriber
    subscriber = get_object_or_404(Subscriber, unid=s_uuid)

    # Get domain
    sender_domain = get_object_or_404(SenderDomain, unid=domain)

    # Context
    mail, domain = subscriber.email.split("@")
    context = {
        "partial_address" : "{}***{}@{}".format(mail[0], mail[-1], domain),
        "domain" : sender_domain.domain
    }

    if request.method == "GET":
        # First step
        context["step"] = "1"
    elif request.method == "POST":
        # Last step
        context["step"] = "2"
        # Unsubscribe!
        subscriber.unsubscribe(sender_domain, reason="Unsubscribe tramite link: {}".format(request.path))

    return render(request, "unsubscribe.html", context=context)


def viewer(request, newsletter_uuid):
    """Newsletter online viewer."""
    newsletter = get_object_or_404(NewsLetterHandler, unid=newsletter_uuid)

    # Return its mail html body
    return HttpResponse( "<body style=\"background-color: {}; color: {};\">{}</body>".format(
        newsletter.nsmail.body_bg,
        newsletter.nsmail.body_fg,
        newsletter.nsmail.render_html()
    ))


class NewsLetterMetricView(ListView):
    """Newsletter metrics."""
    model = NewsLetterMetric
    paginate_by = 1000
    template_name = 'newslettermetric_list.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            return HttpResponseForbidden()

        self.newsletter_uuid = kwargs.get('newsletter_uuid')

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        qs = qs.filter(handler_id=self.newsletter_uuid)

        return qs
