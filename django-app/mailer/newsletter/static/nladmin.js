(function( $ ) {
    // Timer
    var auto_logout_cl = 0;

    // Handler: reset timer
    auto_logout = function() {
        // Clear
        window.clearTimeout( auto_logout_cl );

        // Set
        auto_logout_cl = window.setTimeout( function() {
            // Logout!
            location.assign( '/logout/' );
        }, 60 * 10 * 1000 );
    }

    $( document ).ready( function() {
        // Bind mousemove function
        auto_logout.call( window );
        $( document.body ).on( 'mousemove keydown', auto_logout );

        // CKEDITOR
        $( 'textarea.ckeditor' ).each( function() {
            CKEDITOR.replace( this, {
	            toolbarGroups: [
	                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
	                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
	                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
	                { name: 'forms' },
	                '/',
	                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
	                { name: 'links' },
	                { name: 'insert' },
	                '/',
	                { name: 'styles' },
	                { name: 'colors' },
	                { name: 'tools' },
	                { name: 'others' },
	                { name: 'about' }
	            ]
	            // NOTE: Remember to leave 'toolbar' property with the default value (null).
            });

            $( 'label[for=' + this.id + ']' ).remove();
        });
    });
})( django.jQuery );
