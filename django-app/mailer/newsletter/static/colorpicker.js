(function($){
    getContentDocument = function() {
	    // CKEditor iFrame
	    var ifr = $('.cke_wysiwyg_frame')[0];

        // Get document node
	    if (ifr)
            return ( ifr.contentDocument || ( ifr.contentWindow ? ifr.contentWindow.document : null ) );
    };

    readColors = function() {
        var d = getContentDocument.call(top);

        // Find body and replace properties
		if (d) $('body', d)
			.css({
				'background-color' : $('#id_body_bg').val() || '#FFFFFF',
				'color' : $('#id_body_fg').val()
			});
    };

    // Update colors on change
	$(document).on('change', 'input[type=color]', readColors);

	// Update colors on startup (500ms delay)
	$(document).ready( function() {
	    setTimeout(
	        function() {
	            readColors.call(top);
	        }, 500 );
	});
})( django.jQuery );
