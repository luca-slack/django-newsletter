(function( $ ) {

    var selector = 'a[href$="jpeg"], a[href$="jpg"], a[href$="png"]';
    var klass = 'preview_tooltip';

    $(document)
      // Show
      .on(
          'mouseenter',
          selector,
          function( e ) {
              $('<img src="' + this.href + '">')
                  .css({
                      'position' : 'fixed',
                      'width'    : '256px',
                      'top'      : e.clientY,
                      'left'     : e.clientX
                  })
                  .addClass( klass )
                  .appendTo(document.body);
          }
      )

      // Hide
      .on(
          'mouseleave',
          selector,
          function( e ) {
              $( '.' + klass ).remove();
          }
      );

    // Replace on load
    $( window ).on( 'load', function() {
        $( selector ).each( function() {
            $( this ).replaceWith(
                $( '<img src="' + this.href + '" width="256px">' )
            )
        });
    });

})( django.jQuery );
