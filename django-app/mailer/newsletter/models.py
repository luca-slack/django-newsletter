# Django
from django.db import models
from django.core import mail
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils import timezone

# Admin log
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.models import LogEntry, CHANGE

# Cloudun
from cloudun.fields import RichTextField, ColorField

# StdImage2
from stdimage import StdImageField

# Newsletter
from django.conf import settings

# Python
import uuid, datetime, re, sys


class UuidModel(models.Model):
    """Model containing a `uuid4` field."""
    unid = models.CharField(default=None, max_length=36, unique=True, db_column="unid", help_text="Identificatore universale.")

    def save(self, *args, **kwargs):
        """Sets unique identifier if not present."""
        if not self.unid:
            self.unid = str(uuid.uuid4())
        super().save(*args, **kwargs)

    class Meta:
        abstract=True


class NewsLetterGroup(models.Model):
    """Recipients list."""
    name = models.CharField(max_length=16, unique=True, help_text="Nome utilizzato per il gruppo", verbose_name="nome")

    def __str__(self):
        return "{}".format(self.name)

    class Meta:
        verbose_name = "gruppo di invio"
        verbose_name_plural = "gruppi di invio"


class SenderDomain(UuidModel):
    """Single mail senders domains."""
    # Mail domain
    domain = models.CharField(max_length=32, help_text="Dominio newsletter. Ogni utente può disattivare la propria iscrizione da questo dominio.", verbose_name="nome")

    # Disabled (cannot be selected in admin)
    disabled = models.BooleanField(default=False, verbose_name="disattivato", help_text="Non mostrare tra i domini selezionabili per l'invio")

    def __str__(self):
        return "{}".format(self.domain)

    class Meta:
        verbose_name = "dominio mittente"
        verbose_name_plural = "domini mittenti"


class NewsLetterMail(models.Model):
    """Mail template for a single message."""
    # Email name
    name = models.CharField(unique=True, max_length=24, help_text="Titolo di questo modello", verbose_name="titolo modello")

    # HTML template
    html = RichTextField(help_text="Contenuto della newsletter", verbose_name="contenuto")

    # Body color
    body_bg = ColorField(blank=True, help_text="Colore di sfondo della mail", verbose_name="colore di sfondo")
    body_fg = ColorField(blank=True, help_text="Colore del testo della mail", verbose_name="colore testo")

    def __str__(self):
        return "{}".format(self.name)

    def clone(self):
        """Clone template."""
        clone = self

        # Generate new name
        new_name = "(copia) {}".format(clone.name)
        new_name = new_name[:24]

        # Avoid conflicts
        while NewsLetterMail.objects.filter(name=new_name).exists():
            new_name = "({}) {}".format( datetime.datetime.now().time(), clone.name )
            new_name = new_name[:24]

        # Rename
        clone.name = new_name

        # Insert
        clone.pk = None
        clone.id = None
        clone.save(force_insert=True)

        # Clone picture
        for picture in self.picture_set.all():
            clone.picture_set.create(picture=picture.picture)

        return clone

    def body_size(self):
        """Returns body size."""
        # Size (bytes)
        sz = len(self.render_html().encode())

        if sz > 1024:
            return "{} kilobytes".format(round( float(sz) / float(1024), 2 ))
        else:
            return "{} bytes".format(sz)
    body_size.short_description = "dimensione"

    def render_html(self):
        """Serialize to xhtml."""
        return "{}".format(self.html)

    class Meta:
        verbose_name = "modello email"
        verbose_name_plural = "modelli email"


class Subscriber(UuidModel):
    """A subscriber."""
    # Mail address
    email = models.EmailField(unique=True, help_text="Indirizzo email a cui inviare le newsletter.", verbose_name="indirizzo email")

    # Mailer Groups
    groups = models.ManyToManyField(NewsLetterGroup, blank=True, help_text="Ricevi newsletter dai gruppi qui selezionati.", verbose_name="gruppi", related_name="subscribers")

    # Disabled domains
    nodomains = models.ManyToManyField(SenderDomain, blank=True, help_text="Le newsletter dei gruppi di invio nel box di destra non verranno recapitate a questo iscritto", verbose_name="non ricevere da", related_name="nodomains")

    # Exists flag
    exists = models.BooleanField(default=True, help_text="Nessuna newsletter verrà inviata se l'indirizzo non è attivo", verbose_name="indirizzo attivo")

    # Default manager
    objects = models.Manager()

    def __str__(self):
        return "{}".format(self.email)

    # def log_message(self, message, user=None):
    #     """Logs given message."""
    #     LogEntry.objects.log_action(
    #         user_id         = user.pk, # Always superuser
    #         content_type_id = ContentType.objects.get_for_model(self).pk,
    #         object_id       = self.pk,
    #         object_repr     = self.__str__(),
    #         change_message  = message,
    #         action_flag     = CHANGE
    #     )

    def disable(self, reason="Disattivazione"):
        """Set disabled flag."""
        self.exists = False
        self.save(update_fields=["exists"])

    def unsubscribe(self, domain, reason="Disiscrizione"):
        """Add group to user's blacklist."""
        self.nodomains.add(domain)

    def get_unsubscribe_url(self, sender_domain):
        base_url = settings.BASE_URL
        path = reverse("unsubscribe", args=(sender_domain.unid, self.unid, ))

        return "{}{}".format(base_url, path)

    def save(self, *args, **kwargs):
        """Enforce email format."""
        self.email = self.email.lower()
        if not "@" in self.email:
            raise ValidationError("{} non è un indirizzo email")

        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "iscritto"
        verbose_name_plural = "iscritti"


class NewsLetterHandler(UuidModel):
    """NewsLetter handler."""
    # Canonical name
    cname = models.CharField(max_length=24, help_text="Nome mittente utilizzato durante l'invio", verbose_name="nome visualizzato")

    # Sender email address
    sender_name = models.EmailField(max_length=24, help_text="Indirizzo email del mittente (completo)", verbose_name="mittente")

    # Sender domain
    sender_domain = models.ForeignKey(SenderDomain, help_text="Dominio utilizzato per l'invio", on_delete=models.PROTECT, verbose_name="dominio")

    # Mail subject
    subject = models.CharField(max_length=150, help_text="Oggetto della mail", verbose_name="oggetto")

    # Queued status
    queued = models.NullBooleanField(help_text="Stato della coda", verbose_name="attivo")

    # NewsLetterMail object
    nsmail = models.ForeignKey(NewsLetterMail, help_text="Modello da utilizzare per l'invio", on_delete=models.PROTECT, verbose_name="modello")

    # Recipient groups
    groups = models.ManyToManyField(NewsLetterGroup, help_text="Gruppi a cui inviare questa newsletter", verbose_name="gruppi di invio", related_name="newsletterhandler_recipients")

    # Real recipients handler
    recipients = models.ManyToManyField(Subscriber, help_text="Destinatari", blank=True, related_name="+")

    # Timestamps
    startdate = models.DateTimeField(auto_now_add=True, help_text="Data creazione di questa newsletter", verbose_name="data creazione")
    enddate = models.DateTimeField(null=True, help_text="Data completamento di questa newsletter", verbose_name="data completamento")

    # Counter
    counter = models.PositiveIntegerField(null=False, default=0, help_text="Numero di persone alle quali questa mail è stata inviata", verbose_name="destinatari gestiti")
    total = models.PositiveIntegerField(null=False, default=0, help_text="Numero stimato di destinatari", verbose_name="destinatari totali")

    # Mail sent per cycle
    mail_rate = models.CharField(max_length=2, help_text="Numero di email da inviare ogni minuto", verbose_name="frequenza invio", choices=(
        ("Normale", (
                ("30", "30 mail per ciclo"),
                ("20", "automatica"),
            )
        ),
    ))

    def __str__(self):
        return "newsletter (da {})".format(self.cname)

    def get_absolute_url(self):
        return "{}{}".format(settings.BASE_URL, reverse("view", args=(self.unid, )))

    def is_completed(self):
        """Returns true if this has already entirely processed."""
        return self.enddate is not None

    def is_new(self):
        """Returns true if this has never been enqueued."""
        return self.queued is None

    def log_message(self, message, user):
        """Log a message for this newsletter."""
        LogEntry.objects.log_action(
            user_id         = user.pk,
            content_type_id = ContentType.objects.get_for_model(self).pk,
            object_id       = self.pk,
            object_repr     = self.__str__(),
            change_message  = message,
            action_flag     = CHANGE
        )

    def get_next_batch(self):
        """Get next N subscribers from current queue, filter only existent addresses."""
        max_recipients = int(self.mail_rate)

        return self.recipients.filter(exists=True)[:max_recipients]

    def queue_status_new(self, user=None):
        """Populate recipients-handler intermediary table

        It reads all the subscribers of current instance's groups.
        This newsletter will be re-sent from start."""
        # Alreay done, skip
        if self.is_completed() and user is not None:
            self.log_message("Errore durante l'accodamento: questa newsletter è già stata completamente inviata", user)
            return

        total = 0

        # Get all groups
        for group in self.groups.all():
            # Get all subscribers within the current group, filter valid, and exclude unregistered
            for subscriber in group.subscribers.filter(exists=True):
                if not subscriber.nodomains.filter(id=self.sender_domain.id).exists():
                    self.recipients.add(subscriber)
                    total = total + 1

        # Reset enddate, counter and total values
        self.enddate = None
        self.counter = 0
        self.total = total

        # Set queue status to active
        self.save(update_fields=["total", "counter", ])

        if user:
            self.log_message("Reset - invio da zero", user)

    def queue_status_active(self, user):
        """Mark this handler's queue as active."""
        # Alreay done, skip
        if self.is_completed():
            self.log_message("Errore durante l'avvio: questa newsletter è già stata completamente inviata", user)
            return

        # True means the queue will be processed
        self.queued = True
        self.save(update_fields=["queued", ])
        self.log_message("Stato invio: attivo", user)

    def queue_status_complete(self):
        """Mark this handler's queue as completed."""
        # Set completion time
        self.enddate = timezone.now()

        # False means the queue won't be processed
        self.queued = False
        self.save(update_fields=["enddate", "queued", ])

    def queue_status_paused(self, user):
        """Mark this handler's queue as paused."""
        # Alreay done, skip
        if self.is_completed():
            return

        # False means the queue won't be processed
        self.queued = False
        self.save(update_fields=["queued",])
        self.log_message("Stato invio: in pausa", user)

    def queue_process(self):
        """Process one batch of recipients at once."""

        # Security check
        if self.is_completed():
            return

        # Get next subscribers block from current queue
        current_batch = self.get_next_batch()

        # Is queue empty?
        if len( current_batch ) == 0:
            self.queue_status_complete()
            return

        # Build "from" header
        mail_from = "{} <{}>".format(self.cname, self.sender_name)

        # Absolute url
        view_online_link = self.get_absolute_url()

        # Read variables from database
        disclaimer = _config("disclaimer")
        pre_unsub_link = _config("pre_unsub_link")
        unsub_link = _config("unsub_link")
        ascii_body = "{} {}".format( _config("txt_mail"), view_online_link )

        # SMTP connection
        smtp_connection = mail.get_connection()
        smtp_connection.open()

        for recipient in current_batch:
            # Build mail part
            e = mail.EmailMultiAlternatives(self.subject,
                                            ascii_body,
                                            mail_from,
                                            (recipient.email, ) )

            # Unsubscribe link
            unsubscribe_url = recipient.get_unsubscribe_url(self.sender_domain)
            unsubscribe_link = "{}<br>{} <a href=\"{}\">{}</a>.".format(disclaimer, pre_unsub_link, unsubscribe_url, unsub_link)

            # Attach HTML content
            e.attach_alternative("\n<div style=\"background-color: {}; color: {};\">{}</div><p style=\"text-align:justify;font-size:10px\">{}</p>\n".format(
                    self.nsmail.body_bg,
                    self.nsmail.body_fg,
                    self.nsmail.render_html(),
                    unsubscribe_link
                ), "text/html")

            try:
                smtp_connection.send_messages([e])
                self.counter = self.counter + 1
                self.save(update_fields=('counter',))
                NewsLetterMetric.register_success(self, recipient, "Mail inviata")
            except Exception as err:
                NewsLetterMetric.register_error(self, recipient, "Errore: {}".format(str(err)))
                mail.mail_admins("Exception in Mailer", str(sys.exc_info()[0]), fail_silently=True)

            # Deque
            self.recipients.remove(recipient)

        # Done
        smtp_connection.close()

    @classmethod
    def execute_cron(cls, *args, **options):
        handlers = cls.objects.filter(queued=True)

        for newsletter_handler in handlers:
            if newsletter_handler.is_new():
                # This has never been enqueued
                newsletter_handler.queue_status_new()
            else:
                newsletter_handler.queue_status_new()

            # Process batch
            newsletter_handler.queue_process()

    class Meta:
        verbose_name = "newsletter"
        verbose_name_plural = "newsletter"


class Picture(models.Model):
    """Private uploaded pictures."""
    picture = StdImageField(upload_to="pictures", variations={
                            "thumbnail": (256, 192, True, ),
                            "image": (1600, 1200, ),
                        })
    template = models.ForeignKey("NewsLetterMail", on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return "immagine"

    class Meta:
        verbose_name = "immagini"
        verbose_name_plural = "immagini"


class NewsLetterMetric(models.Model):
    """Simple data pointer for metrics."""

    handler = models.ForeignKey(NewsLetterHandler, on_delete=models.CASCADE)

    timestamp = models.DateTimeField(auto_now_add=True)

    subscriber = models.ForeignKey(Subscriber, null=True, on_delete=models.SET_NULL)

    message = models.TextField()

    flag = models.IntegerField(choices=(
        (0, "Ok", ),
        (1, "Error", ),
    ))

    @classmethod
    def register_error(cls, newsletter_handler, subscriber, message):
        """Register a metric of type ERROR."""
        metric = cls(handler=newsletter_handler, subscriber=subscriber, flag=1, message=message)
        metric.save()

    @classmethod
    def register_success(cls, newsletter_handler, subscriber, message):
        """Register a metric of type SENT."""
        metric = cls(handler=newsletter_handler, subscriber=subscriber, flag=0, message=message)
        metric.save()

    class Meta:
        ordering = ["-timestamp"]
        verbose_name = "metrica"
        verbose_name_plural = "metriche"


class Config(models.Model):
    """Configuration object."""

    var = models.CharField(max_length=16, help_text="Nome configurazione", unique=True)
    value = models.TextField(blank=True, help_text="Valore", verbose_name="valore")

    def __str__(self):
        return "{}".format(self.var)

    class Meta:
        verbose_name = "configurazione"
        verbose_name_plural = "configurazioni"


def _config(var_name):
    try:
        c = Config.objects.get(var=var_name)
    except:
        return ""

    return c.value


# Luca 2013-03-13: queste header ROMPONO IL CAZZO e rendono la posta marcata come indesiderata
# headers = {
#     "Precedence" : "bulk",
#     "List-Help" : "<mailto:postmaster@cloudun.org>",
#     "List-Subscribe" : "<mailto:postmaster@cloudun.org>",
#     "List-Unsubscribe" : "<%s>" % unsubscribe_url,
#     "List-POST" : "NO",
#     "List-Owner" : "sys@cloudun.org",
#     "List-Archive" : "<none>",
#     "List-Unsubscribe" : "<%s>" % unsubscribe_url,
#     "X-Unsubscribe-Mail" : "sys@cloudun.org",
#     "X-Mailer" : "Cloudun.org Newsletter System",
#     "X-CAN-SPAM-1" : "This message is (or may be) a solicitation or advertisement within the specific meaning of the CAN-SPAM Act of 2003.",
#     "X-CAN-SPAM-2" : "You can decline to receive further email from this list ("commercial" and otherwise) by following the instructions in the body of the email or by using the resources in the List-Unsubscribe, X-Unsubscribe-email and X-Unsubscribe-Web email headers.",
#     "X-CAN-SPAM-3" : "My physical postal address is: via Modena 2, Sesto San Giovanni, Milan, Italy",
#     "X-CAN-SPAM-4" : "For more information on these notices see: http://www.cloudun.org/spam"
# }
