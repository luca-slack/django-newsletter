from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.html import format_html
from mailer.settings import STATIC_URL
from newsletter import models


class NewsLetterGroupAdmin(admin.ModelAdmin):
    """ModelAdmin for NewsLetterGroup."""
    actions = ("group_import", "import_and_disable", )
    list_display = ("name", "subscribers_length", "print_export_link", )
    ordering = ("name", )

    def subscribers_length(self, obj):
        """Number of subscribers."""
        return obj.subscribers.count()
    subscribers_length.short_description = "iscritti"

    def print_export_link(self, obj):
        """Link to export page."""
        return format_html("<a href=\"{}\">excel</a>",
            reverse("bulk_export", args=(obj.pk,))
        )
    print_export_link.short_description = "esporta"

    def group_import(self, request, queryset):
        """Redirect to bulk import page."""
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        groups_id = ",".join(selected)
        return HttpResponseRedirect("{}?groups_id={}".format(reverse("bulk_import"), groups_id))
    group_import.short_description = "Importa indirizzi email"

    def import_and_disable(self, request, queryset):
        """Redirect to bulk disable page."""
        return HttpResponseRedirect(reverse("bulk_disable"))
    import_and_disable.short_description = "Importa e disattiva"


class SubscriberAdmin(admin.ModelAdmin):
    """ModelAdmin for Subscriber."""
    fields = (("email", "exists",), "groups", "nodomains", )
    filter_horizontal = ("groups", "nodomains", )
    list_display = ("email", "exists", )
    list_filter = ("exists", )
    search_fields = ("email", )


class NewsLetterHandlerAdmin(admin.ModelAdmin):
    """ModelAdmin for NewsLetterHandler."""
    actions = ("start", "reset", "pause", )
    date_hierarchy = "startdate"
    exclude = ("recipients", "queued", "total", "counter", "unid", )
    filter_horizontal = ("groups", )
    list_display = ("subject", "queued", "startdate", "enddate", "percentage", "preview", )
    ordering = ("-startdate", )
    readonly_fields = ("startdate", "enddate", )

    def preview(self, obj):
        """Preview anchor for admin."""
        return format_html("<a href=\"{}\" target=\"blank\">{}</a>", obj.get_absolute_url(), "visualizza")
    preview.short_description = "anteprima"

    def percentage(self, obj):
        """Percentage of sent messages."""
        return format_html("<a href=\"{}\" target=\"_blank\">{} su {} &#128269;</a>",
            reverse("metrics", kwargs={ "newsletter_uuid": obj.pk }),
            obj.counter,
            obj.total)
    percentage.short_description = "avanzamento"

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "nsmail":
            # Richiesta Federico 2014-04-28
            kwargs["queryset"] = models.NewsLetterMail.objects.order_by("name")
        elif db_field.name == "sender_domain":
            # Richiesta Federico 2015-05-14
            kwargs["queryset"] = models.SenderDomain.objects.filter(disabled=False)

        return super(NewsLetterHandlerAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def start(self, request, queryset):
        """Start newsletter action."""
        for newsletter in queryset:
            newsletter.queue_status_active(request.user)
    start.short_description = "INVIA"

    def reset(self, request, queryset):
        """Restart newsletter again."""
        for newsletter in queryset:
            newsletter.queue_status_new(user=request.user)
    reset.short_description = "RESET - REINVIA A TUTTI"

    def pause(self, request, queryset):
        """Pause newsletter action."""
        for newsletter in queryset:
            newsletter.queue_status_paused(request.user)
    pause.short_description = "PAUSA"


class PictureAdmin(admin.ModelAdmin):
    """ModelAdmin for Pictures."""
    def imgpreview(self, obj):
        return format_html("<img src=\"{}\">", obj.picture.thumbnail.url)
    imgpreview.short_description = "anteprima"

    def link(self, obj):
        return "{}".format(obj.picture.url)

    list_display = ("__str__", "imgpreview", "link", "template", )


class PictureInline(admin.TabularInline):
    """Inline ModelAdmin for Pictures."""
    model = models.Picture
    extra = 3


class NewsLetterMailAdmin(admin.ModelAdmin):
    """ModelAdmin for NewsletterMail."""
    actions = ( "clone_selected", )
    inlines = ( PictureInline, )
    list_display = ("name", "body_size", )

    def clone_selected(self, request, queryset):
        """Clone selected templates."""
        cloned = 0

        for obj in queryset:
            if obj.clone() is not None:
                cloned = cloned + 1

        modeladmin.message_user(request, "Elementi clonati: {}".format(cloned))
    clone_selected.short_description = "Clona selezionati"

    class Media:
        js = [
            STATIC_URL + "colorpicker.js",
            STATIC_URL + "pictures.js",
            STATIC_URL + "nladmin.js",
        ]


class SenderDomainAdmin(admin.ModelAdmin):
    """ModelAdmin for SenderDomain."""
    list_display = ("__str__", "disabled", "unid", )
    readonly_fields = ("unid",)


class ConfigAdmin(admin.ModelAdmin):
    """ModelAdmin for Config."""
    list_display = ("var", "value",)


admin.site.register(models.Config, ConfigAdmin)
admin.site.register(models.NewsLetterGroup, NewsLetterGroupAdmin)
admin.site.register(models.NewsLetterHandler, NewsLetterHandlerAdmin)
admin.site.register(models.NewsLetterMail, NewsLetterMailAdmin)
admin.site.register(models.Picture, PictureAdmin)
admin.site.register(models.Subscriber, SubscriberAdmin)
admin.site.register(models.SenderDomain, SenderDomainAdmin)
