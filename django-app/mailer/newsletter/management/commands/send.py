from django.core.management.base import BaseCommand
from newsletter.models import NewsLetterHandler

class Command(BaseCommand):
    def handle(self, *args, **options):
        NewsLetterHandler.execute_cron()
