from django.core import mail
from django.core.exceptions import ValidationError
from django.contrib.auth import models as auth_models
from django.test import TestCase, TransactionTestCase
from newsletter import models


def create_newsletter_mail(**kwargs):
    defaults = {
        "name": "template 1",
        "html": "<p>Hello</p>",
        "body_bg": "#000000",
        "body_fg": "#FFFFFF",
    }

    defaults.update(kwargs)

    return models.NewsLetterMail.objects.create(**defaults)

def create_subscriber(email="test@example.com", exists=True):
    return models.Subscriber.objects.create(email=email, exists=exists)


class ConfigTestCase(TestCase):
    """Tests configuration getters."""

    def setUp(self):
        # Create instance
        models.Config.objects.create(var="Site Title", value="Mailer test")

    def test_config_getter(self):
        """Configuration are correctly read"""
        var = "Site Title"
        value = "Mailer test"
        self.assertEqual(models._config(var), value)


class NewsLetterMailTestCase(TestCase):
    """Tests NewsLetterMail model."""

    def setUp(self):
        # Create instance
        self.newsletter_mail = create_newsletter_mail()
        self.newsletter_mail.picture_set.create(picture="dummy.jpeg")

    def test_picture_set(self):
        """Test instance has pictures"""
        self.assertEqual(self.newsletter_mail.picture_set.count(), 1)

    def test_body_size(self):
        """Test body size is correctly reported"""
        self.assertEqual(self.newsletter_mail.body_size(), "12 bytes")

    def test_render_html(self):
        """Test html is correctly rendered"""
        self.assertEqual(self.newsletter_mail.render_html(), "<p>Hello</p>")

    def test_clone(self):
        """Tests cloning a NewsLetterMail instance."""
        clone = self.newsletter_mail.clone()

        self.assertEqual(clone.name, "(copia) template 1")
        self.assertEqual(clone.html, "<p>Hello</p>")
        self.assertEqual(clone.picture_set.count(), self.newsletter_mail.picture_set.count())


class SubscriberTestCase(TransactionTestCase):
    """Tests Subscriber model."""

    def setUp(self):
        # Create instance
        self.subscriber = create_subscriber()

    def test_properties(self):
        """Test default properties are correctly set"""
        subscriber = self.subscriber

        self.assertTrue(subscriber.exists)
        self.assertEqual(len(subscriber.unid), 36)

    def test_disable(self):
        """Test disabling a subscriber correctly set its `exists` value to `False`"""
        subscriber = self.subscriber
        subscriber.disable()

        self.assertFalse(subscriber.exists)

    def test_unsubscribe(self):
        """Test unsubscribing from a domain"""
        subscriber = self.subscriber
        domain = models.SenderDomain.objects.create(domain="My test domain")

        subscriber.unsubscribe(domain, reason="Testing unsubscribe")
        is_set = subscriber.nodomains.filter(pk=domain.pk).exists()

        self.assertTrue(is_set)

    def test_unsubscribe_url(self):
        """Test unsubscribe url contains both domain and user's unid"""
        subscriber = self.subscriber
        domain = models.SenderDomain.objects.create(domain="My test domain")

        url = subscriber.get_unsubscribe_url(domain)

        self.assertIn(subscriber.unid, url)
        self.assertIn(domain.unid, url)

    def test_validation(self):
        """Test errors occur in the case the email address does not contain @"""
        with self.assertRaises(ValidationError) as cm:
            models.Subscriber.objects.create(email="invalid.email.address")

class NewsLetterHandlerTestCase(TransactionTestCase):
    """Test NewsLetterHandler model."""

    def setUp(self):
        # Create foreign objects
        newsletter_mail = create_newsletter_mail()
        sender_domain = models.SenderDomain.objects.create(domain="My test domain")

        # Create instance
        props = {
            "cname": "Newsletter test",
            "sender_name": "test@example.com",
            "sender_domain": sender_domain,
            "subject": "Newsletter test subject",
            "nsmail": newsletter_mail,
            "mail_rate": 20,
        }
        self.newsletter = models.NewsLetterHandler.objects.create(**props)

        # Create groups with recipients
        group_0 = models.NewsLetterGroup.objects.create(name="Test group 0")
        group_1 = models.NewsLetterGroup.objects.create(name="Test group 1")

        group_0.subscribers.add(
            create_subscriber(email="user_000@example.com"),
            create_subscriber(email="user_001@example.com"),
            create_subscriber(email="user_002@example.com"),
            create_subscriber(email="user_004@example.com", exists=False)
        )

        group_1.subscribers.add(
            create_subscriber(email="user_005@example.com", exists=False),
            create_subscriber(email="user_006@example.com",),
            create_subscriber(email="user_007@example.com")
        )

        self.newsletter.groups.add(group_0, group_1)

        # Create user for testing
        self.user = auth_models.User.objects.create_user(username='test_user_01', password='abcdef')

    def test_properties(self):
        """Test default properties are correctly set"""
        newsletter = self.newsletter

        self.assertEqual(newsletter.counter, 0)
        self.assertEqual(newsletter.total, 0)
        self.assertIs(newsletter.enddate, None)
        self.assertIsNot(newsletter.startdate, None)
        self.assertEqual(len(newsletter.unid), 36)
        self.assertEqual(newsletter.mail_rate, 20)

    def test_groups(self):
        """Test groups are correctly set."""
        self.assertEqual(self.newsletter.groups.count(), 2)

    def test_unsubscribe(self):
        """Test unsubscribed people are not included in recipients list."""
        newsletter = self.newsletter
        emails = (
            "user_008@example.com",
            "user_009@example.com",
            "user_010@example.com",
            "user_011@example.com",
        )

        # Create Subscriber objects
        people = [ create_subscriber(email=e) for e in emails ]

        # Add people to group and NewsLetter
        group_2 = models.NewsLetterGroup.objects.create(name="Test group 3")
        group_2.subscribers.add(*people)
        newsletter.groups.add(group_2)

        # Unsubscribe all
        for subscriber in people:
            subscriber.unsubscribe(newsletter.sender_domain)

        # Populate queue
        newsletter.queue_status_new(user=self.user)

        # Check absence of people
        self.assertEqual(newsletter.recipients.filter(email__in=emails).count(), 0)

    def test_workflow(self):
        """Test recipients list is correctly populated and mails are sent"""
        newsletter = self.newsletter
        recipients = 5

        # Check queue is not populated
        self.assertEqual(newsletter.recipients.count(), 0)

        # Populate
        newsletter.queue_status_new(user=self.user)

        # Check queue is correctly populated
        self.assertEqual(newsletter.recipients.count(), recipients)

        # Check batch is correctly returned
        self.assertEqual(len(newsletter.get_next_batch()), recipients)

        # Process queue
        newsletter.queue_process()

        # Check queue is now empty
        self.assertEqual(newsletter.recipients.count(), 0)

        # Test that messagex have been sent.
        self.assertEqual(len(mail.outbox), recipients)

        # Verify title is correctly set
        for n in range(0, recipients):
            self.assertEqual(mail.outbox[n].subject, newsletter.subject)

        # Another run
        newsletter.queue_process()

        # Test end date is correctly set
        self.assertIsNot(newsletter.enddate, None)
        self.assertTrue(newsletter.is_completed())

        # Assert counters are correctly updated
        self.assertEqual(newsletter.counter, recipients)
        self.assertEqual(newsletter.total, recipients)
