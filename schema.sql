--
-- Table structure for table `auth_group`
--
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `auth_user`
--
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `django_content_type`
--
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `django_migrations`
--
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `django_session`
--
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_config`
--
CREATE TABLE `newsletter_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `var` varchar(16) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `var` (`var`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_newslettergroup`
--
CREATE TABLE `newsletter_newslettergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_newslettermail`
--
CREATE TABLE `newsletter_newslettermail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) NOT NULL,
  `html` longtext NOT NULL,
  `body_bg` varchar(7) NOT NULL,
  `body_fg` varchar(7) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_picture`
--
CREATE TABLE `newsletter_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture` varchar(100) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `newsletter_picture_template_id_f71f19af_fk_newslette` (`template_id`),
  CONSTRAINT `newsletter_picture_template_id_f71f19af_fk_newslette` FOREIGN KEY (`template_id`) REFERENCES `newsletter_newslettermail` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_senderdomain`
--
CREATE TABLE `newsletter_senderdomain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid4` varchar(36) NOT NULL,
  `domain` varchar(32) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid4` (`uuid4`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_subscriber`
--
CREATE TABLE `newsletter_subscriber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid4` varchar(36) NOT NULL,
  `email` varchar(254) NOT NULL,
  `exists` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid4` (`uuid4`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `auth_permission`
--
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `auth_group_permissions`
--
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `auth_user_groups`
--
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Table structure for table `auth_user_user_permissions`
--
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `django_admin_log`
--
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_newsletterhandler`
--
CREATE TABLE `newsletter_newsletterhandler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid4` varchar(36) NOT NULL,
  `cname` varchar(24) NOT NULL,
  `sender_name` varchar(24) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `queued` tinyint(1) DEFAULT NULL,
  `startdate` datetime(6) NOT NULL,
  `enddate` datetime(6) DEFAULT NULL,
  `counter` int(10) unsigned NOT NULL,
  `total` int(10) unsigned NOT NULL,
  `mail_rate` varchar(2) NOT NULL,
  `nsmail_id` int(11) NOT NULL,
  `sender_domain_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid4` (`uuid4`),
  KEY `newsletter_newslette_nsmail_id_44f0e7ff_fk_newslette` (`nsmail_id`),
  KEY `newsletter_newslette_sender_domain_id_ff49db04_fk_newslette` (`sender_domain_id`),
  CONSTRAINT `newsletter_newslette_nsmail_id_44f0e7ff_fk_newslette` FOREIGN KEY (`nsmail_id`) REFERENCES `newsletter_newslettermail` (`id`),
  CONSTRAINT `newsletter_newslette_sender_domain_id_ff49db04_fk_newslette` FOREIGN KEY (`sender_domain_id`) REFERENCES `newsletter_senderdomain` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_newsletterhandler_groups`
--
CREATE TABLE `newsletter_newsletterhandler_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletterhandler_id` int(11) NOT NULL,
  `newslettergroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_newsletterhan_newsletterhandler_id_new_22758c5d_uniq` (`newsletterhandler_id`,`newslettergroup_id`),
  KEY `newsletter_newslette_newslettergroup_id_99534045_fk_newslette` (`newslettergroup_id`),
  CONSTRAINT `newsletter_newslette_newslettergroup_id_99534045_fk_newslette` FOREIGN KEY (`newslettergroup_id`) REFERENCES `newsletter_newslettergroup` (`id`),
  CONSTRAINT `newsletter_newslette_newsletterhandler_id_2a658b65_fk_newslette` FOREIGN KEY (`newsletterhandler_id`) REFERENCES `newsletter_newsletterhandler` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_newsletterhandler_recipients`
--
CREATE TABLE `newsletter_newsletterhandler_recipients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletterhandler_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_newsletterhan_newsletterhandler_id_sub_b2aca03a_uniq` (`newsletterhandler_id`,`subscriber_id`),
  KEY `newsletter_newslette_subscriber_id_96aed062_fk_newslette` (`subscriber_id`),
  CONSTRAINT `newsletter_newslette_newsletterhandler_id_a224c022_fk_newslette` FOREIGN KEY (`newsletterhandler_id`) REFERENCES `newsletter_newsletterhandler` (`id`),
  CONSTRAINT `newsletter_newslette_subscriber_id_96aed062_fk_newslette` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_newslettermetric`
--
CREATE TABLE `newsletter_newslettermetric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime(6) NOT NULL,
  `message` longtext NOT NULL,
  `flag` int(11) NOT NULL,
  `handler_id` int(11) NOT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `newsletter_newslette_handler_id_42ae1259_fk_newslette` (`handler_id`),
  KEY `newsletter_newslette_subscriber_id_9d37624b_fk_newslette` (`subscriber_id`),
  CONSTRAINT `newsletter_newslette_handler_id_42ae1259_fk_newslette` FOREIGN KEY (`handler_id`) REFERENCES `newsletter_newsletterhandler` (`id`),
  CONSTRAINT `newsletter_newslette_subscriber_id_9d37624b_fk_newslette` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_subscriber_groups`
--
CREATE TABLE `newsletter_subscriber_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_id` int(11) NOT NULL,
  `newslettergroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_subscriber_gr_subscriber_id_newsletter_ad7480db_uniq` (`subscriber_id`,`newslettergroup_id`),
  KEY `newsletter_subscribe_newslettergroup_id_b5db2c74_fk_newslette` (`newslettergroup_id`),
  CONSTRAINT `newsletter_subscribe_newslettergroup_id_b5db2c74_fk_newslette` FOREIGN KEY (`newslettergroup_id`) REFERENCES `newsletter_newslettergroup` (`id`),
  CONSTRAINT `newsletter_subscribe_subscriber_id_e8f534ad_fk_newslette` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `newsletter_subscriber_nodomains`
--
CREATE TABLE `newsletter_subscriber_nodomains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_id` int(11) NOT NULL,
  `senderdomain_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_subscriber_no_subscriber_id_senderdoma_4222cf13_uniq` (`subscriber_id`,`senderdomain_id`),
  KEY `newsletter_subscribe_senderdomain_id_ae5dac61_fk_newslette` (`senderdomain_id`),
  CONSTRAINT `newsletter_subscribe_senderdomain_id_ae5dac61_fk_newslette` FOREIGN KEY (`senderdomain_id`) REFERENCES `newsletter_senderdomain` (`id`),
  CONSTRAINT `newsletter_subscribe_subscriber_id_a760d54a_fk_newslette` FOREIGN KEY (`subscriber_id`) REFERENCES `newsletter_subscriber` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_migrations`
--
LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-03-18 00:40:28.714492'),(2,'auth','0001_initial','2018-03-18 00:40:28.958785'),(3,'admin','0001_initial','2018-03-18 00:40:29.027617'),(4,'admin','0002_logentry_remove_auto_add','2018-03-18 00:40:29.039068'),(5,'contenttypes','0002_remove_content_type_name','2018-03-18 00:40:29.107037'),(6,'auth','0002_alter_permission_name_max_length','2018-03-18 00:40:29.119853'),(7,'auth','0003_alter_user_email_max_length','2018-03-18 00:40:29.139544'),(8,'auth','0004_alter_user_username_opts','2018-03-18 00:40:29.152441'),(9,'auth','0005_alter_user_last_login_null','2018-03-18 00:40:29.182313'),(10,'auth','0006_require_contenttypes_0002','2018-03-18 00:40:29.186277'),(11,'auth','0007_alter_validators_add_error_messages','2018-03-18 00:40:29.200976'),(12,'auth','0008_alter_user_username_max_length','2018-03-18 00:40:29.247632'),(13,'auth','0009_alter_user_last_name_max_length','2018-03-18 00:40:29.262659'),(14,'sessions','0001_initial','2018-03-18 00:40:29.288110');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `django_content_type`
--
LOCK TABLES `django_content_type` WRITE;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(14,'newsletter','config'),(7,'newsletter','newslettergroup'),(11,'newsletter','newsletterhandler'),(9,'newsletter','newslettermail'),(13,'newsletter','newslettermetric'),(12,'newsletter','picture'),(8,'newsletter','senderdomain'),(10,'newsletter','subscriber'),(6,'sessions','session');
UNLOCK TABLES;

--
-- Dumping data for table `auth_permission`
--
LOCK TABLES `auth_permission` WRITE;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add gruppo di invio',7,'add_newslettergroup'),(20,'Can change gruppo di invio',7,'change_newslettergroup'),(21,'Can delete gruppo di invio',7,'delete_newslettergroup'),(22,'Can add dominio mittente',8,'add_senderdomain'),(23,'Can change dominio mittente',8,'change_senderdomain'),(24,'Can delete dominio mittente',8,'delete_senderdomain'),(25,'Can add modello email',9,'add_newslettermail'),(26,'Can change modello email',9,'change_newslettermail'),(27,'Can delete modello email',9,'delete_newslettermail'),(28,'Can add iscritto',10,'add_subscriber'),(29,'Can change iscritto',10,'change_subscriber'),(30,'Can delete iscritto',10,'delete_subscriber'),(31,'Can add newsletter',11,'add_newsletterhandler'),(32,'Can change newsletter',11,'change_newsletterhandler'),(33,'Can delete newsletter',11,'delete_newsletterhandler'),(34,'Can add immagini',12,'add_picture'),(35,'Can change immagini',12,'change_picture'),(36,'Can delete immagini',12,'delete_picture'),(37,'Can add metrica',13,'add_newslettermetric'),(38,'Can change metrica',13,'change_newslettermetric'),(39,'Can delete metrica',13,'delete_newslettermetric'),(40,'Can add configurazione',14,'add_config'),(41,'Can change configurazione',14,'change_config'),(42,'Can delete configurazione',14,'delete_config');
UNLOCK TABLES;
